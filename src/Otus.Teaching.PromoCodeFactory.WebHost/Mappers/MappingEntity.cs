﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class MappingEntity : Profile
    {
        public MappingEntity()
        {
            CreateMap<Customer, CustomerShortResponse>();

            CreateMap<Preference, PreferenceResponse>();
            CreateMap<Customer, CustomerResponse>()
                .ForMember(dto => dto.Preferences,
                    opt =>
                        opt.MapFrom(x => x.Preferences.Select(y => y.Preference).ToList()));


            CreateMap<CreateOrEditCustomerRequest, Customer>();
        }
    }
}