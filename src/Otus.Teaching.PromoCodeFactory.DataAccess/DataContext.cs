﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext: DbContext
    {
        public DbSet<Role> Roles { get; set; }
        
        public DbSet<Employee> Employees { get; set; }
        
        public DbSet<Customer> Customers { get; set; }
        
        public DbSet<Preference> Preferences { get; set; }
        
        public DbSet<PromoCode> PromoCodes { get; set; }
        
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //create composite key
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new {cp.CustomerId, cp.PreferenceId});
            
            //one-to-many CustomerPreference-Customer
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.Preferences)
                .HasForeignKey(cp => cp.CustomerId);

            //one-to-many CustomerPreference-Preference
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany(p => p.Customers)
                .HasForeignKey(cp => cp.PreferenceId);
            
            //one-to-many PromoCode-Customer
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Customer)
                .WithMany(c => c.PromoCodes)
                .OnDelete(DeleteBehavior.Cascade);
        }

    }
}